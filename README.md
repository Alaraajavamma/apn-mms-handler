# APN + MMS handler
This is a workaround for Pinephone users whose carrier has a separate APN for data and MMS. 

This same script also somewhat improves battery life, because it puts the phone into suspend mode much faster when Phosh locks (e.g. you press the power button to close the screen)

I have tested this with Pinephone Beta and most of the OS that use Phosh (PMOS, Mobian, Arch, Manjaro)

To find out more about the original issue see:

https://gitlab.com/kop316/mmsd/-/issues/5

https://gitlab.freedesktop.org/NetworkMa...issues/958

## Description
If your carrier has different APN for data and MMS you can't receive MMS messages if you are using data APN and you can't use internet if you are using MMS APN. 

What this script does? 

It monitors ~/.mms/modemmanager/ folder and when it sees that there is new .status* file it changes to MMS APN. 
Then it starts another script which monitors is the MMS processed correctly (aka. does the mmsd-tng remove .status file like it does when it succeeds). 

It also sends notification what is happening. If there is error it will save "raw mms" files to ~/.mms/error folder and you can try again just moving them back to ~/.mms/modemmanager/


*If there is new .status file you are getting or sending new MMS message -> this file is generated also when you are using data APN but it wil not be processed at all before switching to MMS APN.

## Installation
Most of the depencies are installed by default with most Phosh OSes but gnome-terminal is atleast needed (kgx does not support launching from .desktop files afaik)

In terminal

git clone https://gitlab.gnome.org/Alaraajavamma/apn-mms-handler && cd apn-mms-handler && sudo chmod +x install.sh && sudo chmod +x lock.sh && sudo chmod +x start.sh && sudo chmod +x mms.sh && sudo chmod +x mms2.sh

System Settings you have to set both APNs correctly (seriously correctly - double check). Write down your APN names.

System Settings activate both once and check that they work

System Settings you have to leave the Data APN active

Chatty go to Settings -> SMS and MMS Settings -> and setup correctly MMS Carrier settings. Set these correctly and please double check. Also uncheck "Request delivery report" because otherwise this will cause notification loop with these scripts (atleast with my carrier)

Inside mms.sh and mms2.sh there are these commands which you have to edit to match your carrier APN names:

nmcli c up "dna internet"

nmcli c up "dna mms"

Those are my carrier APN names and (Internet = data and MMS = mms). If your carrier APN name contains only one word you don't need the checkmarks but those are needed when there are two or more words. 

(This is not situation for most people but please notice (thanks b342)! Some carriers tend to have multiply APNs with same APN names. If that is your scenarioa you need to find out your carriers APN "UUID"
How? Select the ones that work from System Settings and when they work open terminal and run 'nmcli con show' that command will show you the UUIDs and what is the active APN UUID. And then you need to replace all "nmcli c up "dna internet" commands and replace them with: 'nmcli c up "UUID"' where UUID is the one what works with data or with mms - remember they are different if you here ;) )

Then run install.sh 
Reboot, login and wait 50 seconds - things should work now but please check :)

Uninstall is easy. Just remove start.desktop from .config/autostart and reboot

## Usage
When you follor "installation" instructions it should just work in the background

## Support
Feel free to open issue but and I can try to help. 
No warranty and that's it ;)

## Roadmap
This is just a workaround and will be total trash when someone smarter invents how Network Manager can handle multiply APN connections same time.
