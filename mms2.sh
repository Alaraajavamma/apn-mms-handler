#!/bin/bash 
	if [ -e ${HOME}/.mms/modemmanager/*.status ]; then 
	notify-send MMS "There was error processing the message" -i email & nmcli c up "dna internet" && sleep 5
	cd ${HOME}/.mms/modemmanager/ && find -maxdepth 1 -mindepth 1 -not -name mms -print0 |xargs -0 mv -t ${HOME}/.mms/error/
	else notify-send MMS "Message processed successfully" -i email && nmcli c up "dna internet" && sleep 5
	fi
