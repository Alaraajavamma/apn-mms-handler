#!/bin/bash
while :
do
	if [ -e ${HOME}/.mms/modemmanager/*.status ]; then 
	notify-send --expire-time=10000 MMS "The message is being processed - please wait" -i email & nmcli c up "dna mms" && sleep 10 && ${HOME}/.mms/mms2.sh && sleep 5
	else sleep 5
	fi
done
