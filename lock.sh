#!/bin/bash
dbus-monitor --session "type='signal',interface='org.gnome.ScreenSaver'" |
 while read x; do
    case "$x" in
    *"boolean true"*) gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-battery-timeout 10 & gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-ac-timeout 10;;
    *"boolean false"*) gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-battery-timeout 60 & gsettings set org.gnome.settings-daemon.plugins.power sleep-inactive-ac-timeout 60;;
    esac
    done
